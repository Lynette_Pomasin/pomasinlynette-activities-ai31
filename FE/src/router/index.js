import Vue from 'vue'
import VueRouter from 'vue-router'
import dashboard from '@/components/dashboard'

Vue.use(VueRouter)

const routes = [
    {
      path: '/',
      name: 'Dashboard',
      component: dashboard
    },
    {
      path: '/patron',
      name: 'Patron Management',
      component: () => import(/* webpackChunkName: "patron" */ '@/components/patron')
    },
    {
      path: '/book',
      name: 'Books Management',
      component: () => import(/* webpackChunkName: "book" */ '@/components/book')
    },
    {
      path: '/settings',
      name: 'Settings',
      component: () => import(/* webpackChunkName: "settings" */ '@/components/settings')
    },
  ]
  
  const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
  })
  
  export default router