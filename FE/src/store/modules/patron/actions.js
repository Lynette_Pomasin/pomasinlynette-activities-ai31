import axios from 'axios';
import API from "@/assets/js/config/axios";

const PATRONS = 'patrons';

const getPatrons = ({commit}) =>{
    axios.get(API + PATRONS)
    .then(response => {
        commit('SET_PATRON', response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}

const updatePatron = ({commit}, {edit, id}) => {
    return new Promise((resolve) => {
        axios.put(API + PATRONS + id, edit)
            .then((response) => {
                commit('UPDATE_PATRON', response.data)
                resolve(response);
            }).catch((error) => {
                resolve(error);
            })
    })
}

const storePatron = ({commit}, add) => {
    return new Promise((resolve) => {
        axios.post(API + PATRONS, add)
            .then((response) => {
                commit('ADD_PATRON', response.data)
                resolve(response);
            }).catch((error) => {
                resolve(error);
            })
    })
}

const deletePatron = ({commit}, id) => {
    return new Promise((resolve) => {
        axios.delete(API+ PATRONS + id)
            .then((response) => {
                commit('DELETE_PATRON', response.data)
                resolve(response);
            }).catch((error) => {
                resolve(error);
            })
    })
}

export default {
	getPatrons,
	updatePatron,
	storePatron,
	deletePatron
}