import * as getters from "./getters";
import * as mutations from "./mutations";
import actions from "./actions";

const state = {
    books: [],
    categories: []
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}