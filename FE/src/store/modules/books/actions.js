import axios from 'axios';
import API from "@/assets/js/config/axios";

const BOOKS = 'books';

const getBooks = ({commit}) =>{
    axios.get(API + BOOKS)
    .then(response => {
        commit('SET_BOOKS', response.data.data)
    }).catch((error) => {
        console.log(error)
    })
}

const storeBook = ({commit}, add) => {
    return new Promise((resolve) => {
        axios.post(API + BOOKS, add)
            .then((response) => {
                commit('ADD_BOOK', response.data)
                resolve(response);
            }).catch((error) => {
                resolve(error);
            })
    })
}

const updateBook = ({commit}, {edit, id}) => {
    return new Promise((resolve) => {
        axios.put(API + BOOKS + id, edit)
            .then((response) => {
                commit('UPDATE_BOOK', response.data)
                resolve(response);
            }).catch((error) => {
                resolve(error);
            })
    })
}

const deleteBook = ({commit}, id) => {
    return new Promise((resolve) => {
        axios.delete(API+ BOOKS + id)
            .then((response) => {
                commit('DELETE_BOOK', response.data)
                resolve(response);
            }).catch((error) => {
                resolve(error);
            })
    })
}

export default {
	getBooks,
	storeBook,
	updateBook,
	deleteBook
}