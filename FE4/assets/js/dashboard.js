
let myFirstChart = document.getElementById('myFirstChart').getContext('2d');
let barChart = new Chart(myFirstChart, {
   	type: 'bar',
    data: {
       	labels: ['Non-Fiction', 'Biography', 'History', 'Mystery', 'Fiction'],
    datasets: [{
        label: 'Books',
        data: [30,25, 30, 15, 28],
        backgroundColor: '#344960',
        borderColor: '#000000',
        borderWidth: 1,
        hoverBorderWidth:2,
        hoverBorderColor: "#fff"
        }]
    },
    options: {
    	title:{
    	   text:'Types of Books Read (October)',
    	   display: true,	
    	   fontSize:15
    	},
    	legend:{
    	   display:false
    	},
       	scales: {
            yAxes: [{
            	scaleLabel:{
            	   labelString: 'Number of Books',
            		display: true,
            		fontSize: 13
            	},
                ticks: {
                    beginAtZero: true
                }
            }]
       	}
    
    }
});

let mySecondChart = document.getElementById('mySecondChart').getContext('2d');
let barChart1 = new Chart(mySecondChart, {
    type: 'bar',
    data: {                       
        labels: ['January', 'Februaruy', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October'],
    datasets: [{
        label: 'Books',
        data: [130, 100, 80, 160, 190, 120, 115, 170, 160, 120],
        backgroundColor: '#344960',
        borderColor: '#000000',
        borderWidth: 1,
        hoverBorderWidth:2,
        hoverBorderColor: "#fff"
        }]
    },
    options: {
        title:{
            text:'Usage of Books',
            display: true,  
            fontSize:15
        },
        legend:{
            display:false
        },
        scales: {
            yAxes: [{
                scaleLabel:{
                    labelString: 'Number of Books',
                    display: true,
                    fontSize: 13
                },
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});