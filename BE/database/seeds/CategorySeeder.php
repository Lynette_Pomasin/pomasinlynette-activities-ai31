<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [['category' => 'Biography'], ['category' => 'Mystery'], ['category' => 'History'], 
            ['category' => 'Fiction'], ['category' => 'Non-Fiction']
        ];

        foreach ($category as $categor) {
            Category::create($categor);
        }
    }
}
