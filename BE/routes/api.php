<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::Resource('patrons', PatronController::class);
Route::Resource('books', BookController::class);
Route::Resource('/borrowedbook', BorrowedBookController::class);
Route::Resource('/returnedbook', ReturnedBookController::class);
Route::get('/categories', [CategoryController::class, 'index']);

