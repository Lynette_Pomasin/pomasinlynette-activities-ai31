<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $fillable = [
    'name',
    'author',
    'copies',
    'category_id'
    ];

    public function book_categories(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
        
    public function book_borrowedBook(){
        return $this->hasMany(BorrowedBook::class, 'book_id', 'id');
    }
        
    public function book_returneBook(){
        return $this->hasMany(ReturnedBook::class, 'book_id', 'id');
    }
    
}
