<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReturnedBookRequest;
use App\Models\BorrowedBook;
use App\Models\ReturnedBook;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ReturnedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(ReturnedBook::with(['book', 'patron', 'book.category'])->get());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReturnedBookRequest $request)
    {
        $borrowed = BorrowedBook::where([['book_id', $request->book_id], ['patron_id', $request->patron_id]])->firstOrFail();

        if(empty($borrowed)) 
        {
            return response()->json(['message' => 'Book not found.'], 404);
        } else {
            if($borrowed->copies == $request->copies) {
                $borrowed->delete();
            }else {
                $borrowed->update(['copies' => $borrowed->copies - $request->copies]);
            }
        }

        $createReturned = ReturnedBook::create($request->all());
        
        $returned = ReturnedBook::with(['book'])->find($createReturned->id);
        $returned->book->update(['copies' => $returned->book->copies + $request->copies]);

        return response()->json(['message' => 'Book returned!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $returned = ReturnedBook::with(['book', 'book.category', 'patron'])->findOrfail($id);
            return response()->json($returned); 
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Returned book not found'], 404);
        }
    }
}
