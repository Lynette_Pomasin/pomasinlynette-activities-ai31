<?php

namespace App\Http\Controllers;

use App\Models\BorrowedBook;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Requests\BorrowedBookRequest;

class BorrowedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(BorrowedBook::with(['patron', 'book', 'book.category'])->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BorrowedBookRequest $request)
    {
        $createBorrowed = BorrowedBook::create($request->only(['book_id', 'copies', 'patron_id']));
        $borrowed = BorrowedBook::with(['book'])->find($createBorrowed->id);
  
        $copies = $borrowed->book->copies - $request->copies;
        $borrowed->book->update(['copies' => $copies]);
  
        return response()->json(['message' => 'Book borrowed']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $borrowed = BorrowedBook::with(['patron', 'book', 'book.category'])->where('id', $id)->firstOrFail();
        return response()->json($borrowed);
    }

}
